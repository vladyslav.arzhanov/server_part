#include<istream>
#include"stdio.h"
#include<fstream>
#include"mqtt/client.h"
#include<thread>
#include<string>
#include<chrono>
#include<iterator>
#include"jsoncpp/json/json.h"
#include<filesystem>
#include"syslog.h"
#include"libconfig.h++"


using namespace std::chrono;

const char* topic="OTA";
libconfig::Config cfg;


std::string getMd5sumstring(std::string path){
	system(("md5sum "+path+" > /tmp/md5file.txt").c_str());
	std::ifstream file("/tmp/md5file.txt");
	std::string ret_val;
	file>>ret_val;
	file.close();
	return ret_val;
}

std::string readUpdate(const std::string& pathWithUpdates) {				
	std::string resJSON;
	for (auto& item : std::filesystem::recursive_directory_iterator(pathWithUpdates)) {
		if (!item.is_directory()) {
				Json::Value head;
				Json::Value data;

				std::string url=cfg.lookup("url");
				url+=item.path().filename().generic_string();			
				data["url"]=url;
				
				data["version"]=item.path().parent_path().filename().generic_string();				
				
				data["md5sum"]=getMd5sumstring(item.path().generic_string());
				
				head[item.path().filename().generic_string()]=data;

				Json::StreamWriterBuilder builder;
				resJSON+=Json::writeString(builder,head);		
		}
	}
	
	if(resJSON.empty()){
	return "{ }";
	syslog(LOG_INFO,"No updates");
	}

	syslog(LOG_INFO,"updates returned to the client");	
	return resJSON;	

}


int main(){
	std::string adress;
	std::string pathWithUpdates;

	try {
		cfg.readFile("server_configuration.cfg");
		adress=cfg.lookup("adress_broker").c_str();
		pathWithUpdates=cfg.lookup("pathWithUpdates").c_str();

		mqtt::client con(adress, "varzhanov");
		openlog("OTA",LOG_PID,LOG_USER);
	
		std::cout << "Connecting to the MQTT server..." << std::flush;
		mqtt::connect_response rsp = con.connect();
		std::cout << "OK" << std::endl;
		syslog(LOG_DEBUG,"%s%s","Successfully connected to mqtt server ",adress.c_str());

		if (!rsp.is_session_present()) {
			std::cout << "Subscribing to topics..." << std::flush;
			con.subscribe(topic, 1);
			std::cout << "OK" << std::endl;
			syslog(LOG_DEBUG,"%s%s%s","Subscribing to topic ",topic," is OK");
		}
		else {
			std::cout << "Session already present. Skipping subscribe." << std::endl;

			syslog(LOG_WARNING,"Session already present. Skipping subscribe.");
		}

		// Consume messages

		while (true) {
			auto msg = con.consume_message();

			if (msg) {
				syslog(LOG_INFO, "%s%s%s",msg->get_topic().c_str() , ": " , msg->to_string().c_str());
				if (msg->get_topic() == "OTA" &&
						msg->to_string() == "ping") {
						syslog(LOG_DEBUG, "Read updates");		
						std::cout<<"Upgrade done!"<<std::endl;
						auto mesg=mqtt::make_message("OTA/responce",readUpdate(pathWithUpdates));
						mesg->set_qos(1);
						con.publish(mesg);

					break;
				}

				std::cout << msg->get_topic() << ": " << msg->to_string() << std::endl;

			}
			else if (!con.is_connected()) {
				std::cout << "Lost connection" << std::endl;
				syslog(LOG_WARNING, "Lost connection");
				while (!con.is_connected()) {
					std::this_thread::sleep_for(milliseconds(250));
				}
				std::cout << "Re-established connection" << std::endl;
				syslog(LOG_INFO, "Re-established connection");
			}
		}
		}
		catch (const mqtt::exception& exc) {
		std::cerr << exc.what() << std::endl;
		syslog(LOG_ERR,"%s%s","Failed connected to mqtt server",adress.c_str());
		return 1;
		}
		catch(...){
		syslog(LOG_ERR,"other failed error in main function");
		}

	closelog();
	return 0;
}
